## VHDL Workflow with git

1. Choose remote repo host (GitLab, GitHub, Bitbucket, etc)
1. Make new project
   - Example: ece480labs for holding all labs
1. Clone project to local machine using provided URL
   - Can be done using command line or visual tool like sourcetree.
1. Set-up .gitignore with below settings
1. Do VHDL stuff
   - Could be built-in text editor of quartus, or external one like VSCode
   - Commit regularly with small snippets
1. Stage files and commit to local copy
1. Push to remote repo after some larger progress made


## .gitignore Recommended Settings
```bash
# Ignore all files except directories
*
!/*

# Allow these file types
!.gitignore

# VHDL files
!*.vhd
!*.vhdl
!*.vht
# Text files
!*.md
!*.txt
```